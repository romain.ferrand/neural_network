import numpy as np

def standard_initializer(layers, rand_gen):
    weights = [rand_gen.randn(y, x) for x, y in zip(layers[:-1], layers[1:])]
    biases = [rand_gen.randn(y, 1) for y in layers[1:]]
    return weights, biases


"""

uniform initializer (Glorot and Bengio (2010))

"""


def uniform_sig_initializer(layers, rand_gen):
    thresholds = [4*(6/inpt+outpt)**0.5 for inpt, outpt in zip(layers[:-1], layers[1:])]
    weights = [rand_gen.uniform(-r, r, (y, x)) for x, y, r in zip(layers[:-1], layers[1:], thresholds)]
    biases = [rand_gen.randn(y, 1) for y in layers[1:]]
    return weights, biases


def uniform_thanh_initializer(layers, rand_gen):
    thresholds = [(6/inpt+outpt)**0.5 for inpt, outpt in zip(layers[:-1], layers[1:])]
    weights = [rand_gen.uniform(-r, r, (y, x)) for x, y, r in zip(layers[:-1], layers[1:], thresholds)]
    biases = [rand_gen.randn(y, 1) for y in layers[1:]]
    return weights, biases


def normalized_variance(layers, rand_gen):
    weights = [rand_gen.randn(y, x)/np.sqrt(x) for x, y in zip(layers[:-1], layers[1:])]
    biases = [rand_gen.randn(y, 1) for y in layers[1:]]
    return weights, biases


def relu_initializer(layers, rand_gen):
    weights = [rand_gen.randn(y, x)*np.sqrt(2/x) for x, y in zip(layers[:-1], layers[1:])]
    biases = [rand_gen.randn(y, 1) for y in layers[1:]]
    return weights, biases