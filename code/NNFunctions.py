import numpy as np


def softmax(x):
    s = np.max(x, axis=0)
    e_x = np.exp(x - s)
    div = np.sum(e_x, axis=0)
    div = div[:, np.newaxis]
    return e_x / div


def softmax_deriv(x):
    y = softmax(x)
    sm = y.reshape((-1, 1))
    return np.diag(x) - np.dot(sm, sm.T)


def cross_entropy_clamp(x, y, eps):
    x=np.clip(x, eps, 1 - eps)
    return np.sum(np.nan_to_num(-y * np.log(x) - (1 - y) * np.log(1 - x)))


sigmoid = {'name': 'sigmoid',
           'id': (lambda x: np.exp(-np.logaddexp(0.0, -x))),
           'deriv': (lambda x: sigmoid['id'](x)*(1 - sigmoid['id'](x)))}

softmax_f = {'name': 'softmax',
            'id': (lambda x: softmax(x)),
            'deriv': (lambda x: softmax_deriv(x))}

L2 = {'name': 'quadratic',
      'id': (lambda x, y: 0.5*np.linalg.norm(x-y)**2),
      'deriv': (lambda x, y: (x - y))}

cross_ent = {'name': 'cross_entropy',
           'id': (lambda x, y, eps=1e-15: cross_entropy_clamp(x, y, eps)),
           'deriv': (lambda x, y: (x - y))}
tanh = {'name': 'tanh',
        'id' : (lambda x: np.tanh(x)),
        'deriv': (lambda x: 1 - np.tanh(x)**2)}

relu = {'name': 'relu',
        'id': (lambda x: np.maximum(0, x)),
        'deriv': (lambda x: x <= 0)}

softplus = {'name': 'softplus',
            'id': (lambda x: (np.log(1 + np.exp(x)))),
            'deriv': (lambda x: sigmoid['id'](x))}