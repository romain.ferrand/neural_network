import numpy as np
import sys


class SGD:
    def __init__(self, lr, reg):
        self.lr = lr
        self.reg = reg

    def to_string(self):
        return 'SGD_{0}_{1}'.format(self.lr, self.reg.to_string())

    def train(self, weights, delta_w, biases, delta_b, nb_entries):
        weights = [current_w - self.lr * new_w - self.lr * self.reg.w(current_w) / nb_entries for
                   current_w, new_w in zip(weights, delta_w)]

        biases = [current_b - self.lr * new_b - self.lr * self.reg.b(current_b) / nb_entries for
                  current_b, new_b in zip(biases, delta_b)]
        return weights, biases

class ADADELTA_MIXED:
    def __init__(self, layers, decay_rates, epsi, reg):
        self.decay_rates = decay_rates
        self.epsi = epsi
        self.reg = reg
        self.w_ac_grad = [[np.zeros((y, x), dtype=float) for x, y in zip(layers[:-1], layers[1:])], ] * len(self.decay_rates)
        self.w_ac_delta = [[np.zeros((y, x), dtype=float) for x, y in zip(layers[:-1], layers[1:])], ] * len(self.decay_rates)
        self.b_ac_grad = [[np.zeros((y, 1), dtype=float) for y in layers[1:]], ] * len(self.decay_rates)
        self.b_ac_delta = [[np.zeros((y, 1), dtype=float) for y in layers[1:]], ] * len(self.decay_rates)

    def to_string(self):
        return 'Adadelta_mixed_{0}_{1}_{2}'.format(self.decay_rates, self.epsi, self.reg.to_string())

    def train(self, weights, delta_w, biases, delta_b, nb_entries):
        w_update = None
        b_update = None
        for i in range(len(self.decay_rates)):
            self.w_ac_grad[i] = [self.decay_rates[i]*w_ac_g + (1 - self.decay_rates[i])*(w_d**2) for w_ac_g, w_d
                           in zip(self.w_ac_grad[i], delta_w)]

            self.b_ac_grad[i] = [self.decay_rates[i]*b_ac_g + (1 - self.decay_rates[i])*(b_d**2) for b_d, b_ac_g
                           in zip(delta_b, self.b_ac_grad[i])]
            temp_w_update = [(-1*(w_ac_d + self.epsi)**0.5 / (w_ac_g + self.epsi)**0.5)*w_d for
                        w_ac_g, w_ac_d, w_d in zip(self.w_ac_grad[i], self.w_ac_delta[i], delta_w)]
            temp_b_update = [(-1 * (b_ac_d + self.epsi)**0.5 / (b_ac_g + self.epsi)**0.5) * b_d for
                        b_ac_g, b_ac_d, b_d in zip(self.b_ac_grad[i], self.b_ac_delta[i], delta_b)]
            if w_update:
                w_update = [np.where(abs(x) > abs(y), x, y) for x, y in zip(w_update, temp_w_update)]
            else:
                w_update = temp_w_update
            if b_update:
                b_update = [np.where(abs(x) > abs(y), x, y) for x, y in zip(b_update, temp_b_update)]
            else:
                b_update = temp_b_update
            self.w_ac_delta[i] = [self.decay_rates[i]*w_ac_d + (1 - self.decay_rates[i])*(w_up**2) for w_ac_d, w_up
                in zip(self.w_ac_delta[i], w_update)]

            self.b_ac_delta[i] = [self.decay_rates[i]*b_ac_d + (1 - self.decay_rates[i])*(b_up**2) for b_ac_d, b_up
                in zip(self.b_ac_delta[i], b_update)]

        weights = [w + d_w - self.reg.w(d_w)/nb_entries for w, d_w in zip(weights, w_update)]
        biases = [b + d_b - self.reg.b(d_b)/nb_entries for b, d_b in zip(biases, b_update)]

        return weights, biases

class ADADELTA:
    def __init__(self, layers, decay_rate, epsi, reg):
        self.decay_rate = decay_rate
        self.epsi = epsi
        self.reg = reg
        self.w_ac_grad = [np.zeros((y, x), dtype=float) for x, y in zip(layers[:-1], layers[1:])]
        self.w_ac_delta = [np.zeros((y, x), dtype=float) for x, y in zip(layers[:-1], layers[1:])]
        self.b_ac_grad = [np.zeros((y, 1), dtype=float) for y in layers[1:]]
        self.b_ac_delta = [np.zeros((y, 1), dtype=float) for y in layers[1:]]

    def to_string(self):
        return 'Adadelta_{0}_{1}_{2}'.format(self.decay_rate, self.epsi, self.reg.to_string())

    def train(self, weights, delta_w, biases, delta_b, nb_entries):
        self.w_ac_grad = [self.decay_rate*w_ac_g + (1 - self.decay_rate)*(w_d**2) for w_ac_g, w_d
                       in zip(self.w_ac_grad, delta_w)]

        self.b_ac_grad = [self.decay_rate*b_ac_g + (1 - self.decay_rate)*(b_d**2) for b_d, b_ac_g
                       in zip(delta_b, self.b_ac_grad)]

        w_update = [(-1*(w_ac_d + self.epsi)**0.5 / (w_ac_g + self.epsi)**0.5)*w_d for
                    w_ac_g, w_ac_d, w_d in zip(self.w_ac_grad, self.w_ac_delta, delta_w)]
        b_update = [(-1 * (b_ac_d + self.epsi)**0.5 / (b_ac_g + self.epsi)**0.5) * b_d for
                    b_ac_g, b_ac_d, b_d in zip(self.b_ac_grad, self.b_ac_delta, delta_b)]

        self.w_ac_delta = [self.decay_rate*w_ac_d + (1 - self.decay_rate)*(w_up**2) for w_ac_d, w_up
            in zip(self.w_ac_delta, w_update)]

        self.b_ac_delta = [self.decay_rate*b_ac_d + (1 - self.decay_rate)*(b_up**2) for b_ac_d, b_up
            in zip(self.b_ac_delta, b_update)]

        weights = [w + d_w - self.reg.w(d_w)/nb_entries for w, d_w in zip(weights, w_update)]
        biases = [b + d_b - self.reg.b(d_b)/nb_entries for b, d_b in zip(biases, b_update)]

        return weights, biases

class ADADELTA_adadecay:
    def __init__(self, layers, epsi, reg):
        self.w_rate = [np.ones((y, x), dtype=float) for x, y in zip(layers[:-1], layers[1:])]
        self.b_rate = [np.ones((y, 1), dtype=float) for y in layers[1:]]
        self.epsi = epsi
        # self.decay_rate= 0.90
        self.reg = reg
        self.w_ac_grad = [np.zeros((y, x), dtype=float) for x, y in zip(layers[:-1], layers[1:])]
        self.w_ac_delta = [np.zeros((y, x), dtype=float) for x, y in zip(layers[:-1], layers[1:])]
        self.b_ac_grad = [np.zeros((y, 1), dtype=float) for y in layers[1:]]
        self.b_ac_delta = [np.zeros((y, 1), dtype=float) for y in layers[1:]]

    def to_string(self):
        return 'Adadelta_{0}_{1}_{2}'.format(self.decay_rate, self.epsi, self.reg.to_string())

    def train(self, weights, delta_w, biases, delta_b, nb_entries):
        self.w_ac_grad = [decay*w_ac_g + (1 - decay)*(w_d**2) for decay, w_ac_g, w_d
                       in zip(self.w_rate, self.w_ac_grad, delta_w)]

        self.b_ac_grad = [decay*b_ac_g + (1 - decay)*(b_d**2) for decay, b_d, b_ac_g
                       in zip(self.b_rate, delta_b, self.b_ac_grad)]

        w_lr = [((w_ac_d + self.epsi)**0.5 / (w_ac_g + self.epsi)**0.5) for
                    w_ac_g, w_ac_d in zip(self.w_ac_grad, self.w_ac_delta)]

        w_max = np.max([np.max(lr) for lr in w_lr]) - sys.float_info.epsilon
        w_min = np.min([np.min(lr) for lr in w_lr]) + sys.float_info.epsilon

        b_lr = [((b_ac_d + self.epsi)**0.5 / (b_ac_g + self.epsi)**0.5) for
                    b_ac_g, b_ac_d in zip(self.b_ac_grad, self.b_ac_delta)]
        b_max = np.max([np.max(lr) for lr in b_lr]) - sys.float_info.epsilon
        b_min = np.min([np.min(lr) for lr in b_lr]) + sys.float_info.epsilon

        w_update = [(-lr)*d_w for lr, d_w in zip(w_lr, delta_w)]
        b_update = [(-lr)*d_b for lr, d_b in zip(b_lr, delta_b)]

        self.w_ac_delta = [decay*w_ac_d + (1 - decay)*(w_up**2) for decay, w_ac_d, w_up
            in zip(self.w_rate, self.w_ac_delta, w_update)]

        self.b_ac_delta = [decay*b_ac_d + (1 - decay)*(b_up**2) for decay, b_ac_d, b_up
            in zip(self.b_rate, self.b_ac_delta, b_update)]
        self.w_rate = [(y - w_max) / (w_min - w_max) for y in w_lr]
        self.b_rate = [(y - b_max) / (b_min - b_max) for y in b_lr]

        weights = [w + d_w - self.reg.w(d_w)/nb_entries for w, d_w in zip(weights, w_update)]
        biases = [b + d_b - self.reg.b(d_b)/nb_entries for b, d_b in zip(biases, b_update)]

        return weights, biases