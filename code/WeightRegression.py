import numpy as np
"""
Package for weight regulation methods   
"""

class NoReg:

    def to_string(self):
        return 'NoReg'
    def w(self, w):
        return w

    def b(self, b):
        return b

    def cost(self, weight, biases):
        return 0


class WeightDecay:

    def __init__(self, ld):
        self.ld = ld

    def to_string(self):
        return 'WeightDecay_{0}'.format(self.ld)

    def w(self, w):
        return self.ld*2*w

    def b(self, b):
        return self.ld*2*b

    def cost(self, weights, biases):
        w_t = 0
        b_t = 0
        for w, b in zip(weights, biases):
            w_t += np.sum(np.dot(w.T, w))
            b_t += np.sum(np.dot(b.T, b))
        return self.ld*(b_t + w_t)


class WeightElimination:
    def __init__(self, ld):
        self.ld = ld

    def to_string(self):
        return 'WeightElimination_{0}'.format(self.ld)

    def w(self, w):
        return self.ld*(2*w / (1+w)**2)

    def b(self, b):
        return self.ld*(2*b / (1+b)**2)

    def cost(self, weights, biases):
        w_sq = 0
        w_sum = 0
        b_sq = 0
        b_sum = 0
        for w, b in zip(weights, biases):
            w_sq += np.sum(np.dot(w.T, w))
            w_sum += np.sum(w) + 1
            b_sq += np.sum(np.dot(b.T, b))
            b_sum += np.sum(b) + 1
        w = self.ld*(w_sq/w_sum)
        b = self.ld*(b_sq/b_sum)
        return w + b
